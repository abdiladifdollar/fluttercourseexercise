import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:math';
import 'package:faker/faker.dart';

class Post {
  String personalimg;
  String Name;
  String txt;
  String postimg;
  DateTime time;

  Post({
    required this.personalimg,
    required this.Name,
    required this.txt,
    required this.postimg,
    required this.time,
  });
}

List<String> usersImage = [
  "assets/images/personimage1.png",
  "assets/images/personimage2.png",
  "assets/images/personimage3.png",
];

final faker = Faker();
final posts = List.generate(3, (index) {
  String randomImage = usersImage[Random().nextInt(usersImage.indexed.length)];
  return Post(
    personalimg: randomImage,
    Name: [
      'Abdiladif Hussein Abdisalan',
      'Abdirahman Farah Yusuf',
      'Abdalle Mohamed Abdinur'
    ][index],
    txt: faker.lorem.sentences(5).toString(),
    postimg: "assets/images/img${index + 1}.png",
    time: DateTime.now().subtract(Duration(minutes: index + 1)),
  );
}).reversed.toList();
