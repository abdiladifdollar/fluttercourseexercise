import 'dart:async';
import 'dart:isolate';

import 'fbpostclass.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'dart:math';
import 'package:faker/faker.dart';
import 'package:shimmer/shimmer.dart';

class MyPost extends StatefulWidget {
  const MyPost({super.key});

  @override
  State<MyPost> createState() => _MyPostState();
}

List<String> usersImage = [
  "assets/images/personimage1.png",
  "assets/images/personimage2.png",
  "assets/images/personimage3.png",
];
List<String> postimage = [
  "assets/images/img1.png",
  "assets/images/img2.png",
  "assets/images/img3.png"
];
final faker = Faker();

class _MyPostState extends State<MyPost> {
  @override
  void initState() {
    Future.delayed(Duration(seconds: 2), () {
      setState(() {
        isLoaded = true;
      });
    });
  }

  bool isLoaded = false;
  final posts = List.generate(3, (index) {
    print(index);
    String randomImage =
        usersImage[Random().nextInt(usersImage.indexed.length)];
    String postpicture = postimage[Random().nextInt(postimage.indexed.length)];
    return Post(
      personalimg: randomImage,
      Name: [
        'Abdiladif Hussein Abdisalan',
        'Abdirahman Farah Yusuf',
        'Abdalle Mohamed Abdinur'
      ][index],
      txt: faker.lorem.sentences(6).toString(),
      postimg: "assets/images/img${index + 1}.png",
      //assets/images/img1.png
      time: DateTime.now().subtract(Duration(minutes: index + 1)),
    );
  }).reversed.toList();
// Emoji stickers

  Widget Emoji(IconData emjicon, String emjtext) {
    return Expanded(
      child: Row(
        children: [],
      ),
    );
  }

// Reaction Buttons Widget
  // Widget buttons(IconData icon, String text) {
  //   return Expanded(
  //       child: ElevatedButton.icon(
  //           onPressed: () {},
  //           icon: Icon(icon),
  //           label: Text(
  //             text,
  //             style: TextStyle(
  //               color: Colors.black54,
  //               fontSize: 15,
  //             ),
  //           ),
  //           style: ElevatedButton.styleFrom(
  //               backgroundColor: Colors.white,
  //               shape: RoundedRectangleBorder(
  //                 borderRadius: BorderRadius.zero,
  //                 side: BorderSide.none,
  //               ))));
  // }

  // Widget buttons(IconData icons, String text) {
  //   return IconButton(
  //     icon: isLiked ? Icon(icons, color: Colors.blue) : Icon(icons),
  //     onPressed: toggleLike,
  //   );
  // }

  int likes = 954;
  bool isLiked = false;
  bool isCommented = false; // Added state for Comment
  bool isShared = false; // Added state for Share

  Widget buttons(IconData icons, bool isActive, VoidCallback onPressed) {
    return IconButton(
      icon: Icon(icons, color: isActive ? Colors.blue : Colors.black),
      onPressed: onPressed,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(179, 255, 255, 255),
      appBar: AppBar(
        title: Center(
            child: Text(
          'Facebook Post',
          style: TextStyle(color: Colors.white),
        )),
        backgroundColor: Color.fromARGB(255, 39, 17, 160),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: !isLoaded
            ? Shimmer.fromColors(
                baseColor: Colors.grey.shade400,
                highlightColor: Colors.green.shade100,
                direction: ShimmerDirection.ltr,
                child: ListView.builder(
                  itemCount: posts.length,
                  itemBuilder: (context, index) {
                    Post postIndex = posts[index];
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ListTile(
                          leading: CircleAvatar(),
                          title: Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 200,
                                  color: Colors.black,
                                  height: 20,
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      width: 35,
                                      color: Colors.black,
                                      height: 12,
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Column(
                              children: [
                                Container(
                                  width: 450,
                                  color: Colors.black,
                                  height: 10,
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Container(
                                  width: 450,
                                  color: Colors.black,
                                  height: 10,
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Container(
                                  width: 450,
                                  color: Colors.black,
                                  height: 10,
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Container(
                                  width: 450,
                                  color: Colors.black,
                                  height: 10,
                                ),
                              ],
                            )),
                        SizedBox(height: 10),
                        Image.asset(
                          postIndex.postimg,
                          width: double.infinity,
                          fit: BoxFit.cover,
                        ),
                        SizedBox(height: 10),
                        Padding(
                          padding: const EdgeInsets.only(left: 7),
                          child: Container(
                            width: 20,
                            height: 6,
                            color: Colors.black,
                          ),
                        ),
                        SizedBox(
                          height: 7,
                        ),
                        Divider(
                          height: 1,
                          thickness: 1,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              // For the Like button
                              Container(
                                width: 30,
                                color: Colors.black,
                                height: 8,
                              ),
                              Container(
                                width: 30,
                                color: Colors.black,
                                height: 8,
                              ),
                              Container(
                                width: 30,
                                color: Colors.black,
                                height: 8,
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  },
                ),
              )
            : ListView.builder(
                itemCount: posts.length,
                itemBuilder: (context, index) {
                  Post postIndex = posts[index];
                  return Card(
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ListTile(
                          leading: ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: Image.asset(
                              postIndex.personalimg,
                              width: 50,
                              height: 50,
                              fit: BoxFit.cover,
                            ),
                          ),
                          title: Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(postIndex.Name),
                                Row(
                                  children: [
                                    Text(
                                      DateFormat("m").format(postIndex.time) +
                                          "m",
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 12),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: Text(
                            postIndex.txt,
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                        SizedBox(height: 10),
                        Image.asset(
                          postIndex.postimg,
                          width: double.infinity,
                          fit: BoxFit.cover,
                        ),
                        SizedBox(height: 10),
                        Padding(
                          padding: const EdgeInsets.only(left: 7),
                          child: Text(
                            '$likes likes', // Display the number of likes here
                            style: TextStyle(color: Colors.grey, fontSize: 14),
                          ),
                        ),
                        SizedBox(
                          height: 3,
                        ),
                        Divider(
                          height: 1,
                          thickness: 1,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              // For the Like button
                              buttons(Icons.thumb_up, isLiked, () {
                                setState(() {
                                  if (isLiked) {
                                    likes--;
                                  } else {
                                    likes++;
                                  }
                                  isLiked = !isLiked;
                                  print(likes);
                                });
                              }),

                              buttons(Icons.comment, isCommented, () {
                                print("Comment button pressed");
                              }),

                              buttons(Icons.share, isShared, () {
                                setState(() {
                                  isShared = !isShared;
                                });
                              }),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
      ),
    );
  }
}
