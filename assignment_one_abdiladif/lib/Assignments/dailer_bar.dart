import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DailerBar extends StatefulWidget {
  const DailerBar({super.key});

  @override
  State<DailerBar> createState() => DailerBarState();
}

class DailerBarState extends State<DailerBar> {
  Widget firstRow(String text, IconData phone, IconData alerm, IconData atain,
      String batterLevel, IconData battery) {
    return Padding(
      padding: const EdgeInsets.only(top: 7),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              SizedBox(width: 16),
              Text(
                text,
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(width: 5),
              Icon(
                phone,
                color: Colors.white,
              ),
            ],
          ),
          Row(
            children: [
              Icon(
                alerm,
                color: Colors.white,
              ),
              Icon(
                atain,
                color: Colors.white,
              ),
              Text(
                batterLevel,
                style: TextStyle(color: Colors.white),
              ),
              Icon(
                battery,
                color: Colors.white,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget secondRow(
      IconData number, IconData phone, String text, IconData threeDots) {
    return Center(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: Row(
          // mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              height: 80,
            ),
            Row(
              children: [
                Icon(
                  number,
                  color: Colors.blue,
                ),
                SizedBox(
                  width: 5,
                ),
                Icon(phone, color: Colors.white),
                SizedBox(
                  width: 5,
                ),
                Text(
                  text,
                  style: TextStyle(color: Colors.white),
                )
              ],
            ),
            Icon(
              threeDots,
              color: Colors.white,
            ),
          ],
        ),
      ),
    );
  }

  Widget callButtons(IconData icon, String text) {
    return Column(
      children: [
        ElevatedButton(
          onPressed: () {},
          child: Icon(
            icon,
          ),
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.transparent,
            // shadowColor: Colors.transparent,
            // disabledForegroundColor: Colors.transparent.withOpacity(0.38),
            // disabledBackgroundColor: Colors.transparent.withOpacity(0.12),
            elevation: 0,
            iconColor: Colors.white,
          ),
        ),
        Text(
          text,
          style: TextStyle(color: Colors.white),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
        ),
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromARGB(255, 0, 0, 0),
                Color.fromARGB(255, 47, 55, 61),
                Color.fromARGB(255, 56, 77, 77),
                Color.fromARGB(255, 62, 54, 70)
              ],
            ),
          ),
          child: Column(
            children: [
              firstRow(
                '3:32',
                Icons.phone,
                Icons.alarm,
                Icons.signal_cellular_alt_outlined,
                '90%',
                Icons.battery_full,
              ),
              secondRow(Icons.looks_one_rounded, Icons.phone, '00:05',
                  Icons.more_vert),
              SizedBox(
                height: 40,
              ),
              Text(
                '2122',
                style: TextStyle(
                    fontSize: 40,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              SizedBox(
                height: 80,
              ),
              Container(
                height: 350,
                width: 350,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  color: Colors.black26,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          callButtons(Icons.add, 'Add Call'),
                          callButtons(Icons.pause, 'Add Call'),
                          callButtons(Icons.bluetooth, 'Add Call')
                        ],
                      ),
                      SizedBox(
                        height: 45,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          callButtons(Icons.volume_up, 'Add Call'),
                          callButtons(Icons.volume_off, 'Add Call'),
                          callButtons(Icons.dialpad, 'Add Call')
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      ElevatedButton(
                        onPressed: () {},
                        child: Icon(Icons.call_end),
                        style: ElevatedButton.styleFrom(
                            iconColor: Colors.white,
                            backgroundColor: Colors.red,
                            shape: CircleBorder(), // Circular shape
                            padding: EdgeInsets.all(20)),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
