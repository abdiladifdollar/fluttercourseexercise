import 'package:flutter/material.dart';
import 'package:flutterassignment/Assignments/dailer_bar.dart';
import 'package:flutterassignment/Assignments/fbpostview.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  _MyHomePage createState() => _MyHomePage();
}

class _MyHomePage extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Home Page'),
        ),
        body: Container(
          color: Colors.black54,
          height: 500,
          width: 500,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MyPost(),
                      ));
                },
                child: Text('Facebook Page'),
              ),
              SizedBox(
                height: 50,
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DailerBar(),
                      ));
                },
                child: Text('Dailer Page'),
              ),
            ],
          ),
        ));
  }
}
